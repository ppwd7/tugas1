from django.shortcuts import render
from fitur_update_status.models import Status
from fitur_add_friend.models import Friend
from tugas1.active_profile import get_active_profile

# Create your views here.

def index(request):
	profile = get_active_profile()
	response = {}

	response['jumlah_status'] = Status.objects.count()
	try:
		response['status_terakhir'] = Status.objects.get_newest()
		response['jumlah_teman'] = Friend.objects.count()
		response['nama'] = profile.name
		response['page_stats'] = True
		response['picture'] = profile.picture_url
	except LookupError:
		pass #biar jadi null



	return render(request, 'stats.html',response)
