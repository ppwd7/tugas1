from django.apps import AppConfig


class FiturHalamanStatistikConfig(AppConfig):
    name = 'fitur_halaman_statistik'
