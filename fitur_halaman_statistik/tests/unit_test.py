from django.test import TestCase,Client

from fitur_update_status.models import Status
from fitur_add_friend.models import Friend
from tugas1.active_profile import get_active_profile

class UnitTest(TestCase):
   

    def setUp(self):
        Status.objects.create(message = "test")

        

    def test_profile_name_correct(self):
        response = Client().get('/stats/')
        html_response = response.content.decode('utf8')
        self.assertIn(get_active_profile().name,html_response)

    def test_amount_of_friends_correct(self):
        response = Client().get('/stats/')
        html_response = response.content.decode('utf8')
        self.assertIn(str(Friend.objects.count()),html_response)
        
    def test_amount_of_status_correct(self):
        response = Client().get('/stats/')
        html_response = response.content.decode('utf8')
        self.assertIn(str(Status.objects.count()),html_response)


    def test_latest_status(self):
        Status.objects.create(message = "erdtfyghdsafutdg")
        response = Client().get('/stats/')
        html_response = response.content.decode('utf8')
        self.assertIn(Status.objects.get_newest().message,html_response)

    def test_no_status(self):
        for status in Status.objects.all():
            status.delete()
        response = Client().get('/stats/')
        html_response = response.content.decode('utf8')
        self.assertIn("No Status Yet",html_response)

