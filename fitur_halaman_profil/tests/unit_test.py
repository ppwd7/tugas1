from datetime import date
from django.test import TestCase, Client
from django.core import validators
from tugas1.active_profile import get_active_profile, create_new_profile
from datetime import date
from tugas1.models import Profile


class UnitTest(TestCase):

    def test_name_is_exist(self):
        profile = get_active_profile()
        response = Client().get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn(profile.name, html_response)

    def test_age_is_exist(self):
        profile = get_active_profile()
        response = Client().get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn(profile.gender, html_response)

    def test_description_is_exist(self):
        profile = get_active_profile()
        response = Client().get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn(profile.description, html_response)

    def test_email_is_exist(self):
        profile = get_active_profile()
        response = Client().get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn(profile.email, html_response)

    def test_picture_is_exist(self):
        profile = get_active_profile()
        response = Client().get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn(profile.picture_url, html_response)
