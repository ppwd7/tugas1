from django.shortcuts import render
from datetime import datetime, date
from tugas1.active_profile import get_active_profile


def index(request):
    profile = get_active_profile()
    response = {'name': profile.name,'description':profile.description, 'gender': profile.gender, 'email' : profile.email, 'birth' : profile.birth_date, 'expertise' : profile.expertise.split(','), 'picture' : profile.picture_url, 'page_profile' : True}
    return render(request, 'profile.html', response)
