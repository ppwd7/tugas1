from django.apps import AppConfig


class FiturHalamanProfilConfig(AppConfig):
    name = 'fitur_halaman_profil'
