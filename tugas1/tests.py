from datetime import date
from django.test import TestCase
from django.core import validators
from .active_profile import get_active_profile, create_new_profile
from datetime import date
from .models import Profile

class UnitTest(TestCase):

    def test_create_new_profile(self):
        create_new_profile()
        self.assertEqual(len(Profile.objects.all()), 1)

        # assert that the created profile is sufficient
        active_profile = Profile.objects.all()[0]
        assert len(active_profile.name) > 5
        validators.validate_email(active_profile.email)
        assert len(active_profile.picture_url) > 4
        assert len(active_profile.expertise) > 3
        min_age = date(2004, 1, 1)
        assert active_profile.birth_date < min_age

    def test_get_active_profile_willCreateNew(self):
        active_profile = get_active_profile()
        assert isinstance(active_profile, Profile)

    def test_get_active_profile_willUseExisting(self):
        existing_profile = Profile.objects.create(name='testProfile', birth_date=date(1, 1, 1))
        self.assertEqual(get_active_profile(), existing_profile)

    def test_Profile_string_representation(self):
        name = 'testName'
        email = 'test@mail.com'
        test_profile = Profile.objects.create(name=name, birth_date=date(1, 1, 1), email=email)
        self.assertEqual(str(test_profile), name + '(' + email + ')')
