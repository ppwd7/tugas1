"""tugas1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import fitur_add_friend.urls as friends
import fitur_halaman_profil.urls as profil
import fitur_update_status.urls as status
import fitur_halaman_statistik.urls as statistik



urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^friend/', include(friends, namespace='friend')),
    url(r'^status/', include(status, namespace='status')),
    url(r'^', include(status, namespace='index')),
    url(r'^stats/', include(statistik, namespace='stats')),
    url(r'^profile/', include(profil, namespace='profile')),

]
