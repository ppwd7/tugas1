from datetime import date
from .models import Profile

# DEFAULT USER
NAME = 'Azhar Kurnia'
EMAIL = 'Azhar.bler@ui.ac.id'
PICTURE_URL = 'https://scontent-sit4-1.cdninstagram.com/t51.2885-19/s320x320/20635216_250013748840572_831614704840343552_a.jpg'
GENDER = 'Male'
DESCRIPTION = 'Saya adalah Penakluk wanita sejati, yang megang fasilkom, nggak pernah nggak pilek, selalu pakai topi'
BIRTH = date(1998, 6, 28)
EXPERTISE = ',Programming,Dota,tidur,Gele,Jualan,Asik-asik,'


def get_active_profile():
    '''
    Returns most recently created profile,
    if none exists, it will create a new one
    '''
    if not Profile.objects.all():
        create_new_profile()
    return Profile.objects.all()[len(Profile.objects.all()) - 1]

def create_new_profile():
    '''
    Creates new profile based on the constants defined on tugas1.active_profile
    '''
    Profile.objects.create(name=NAME, email=EMAIL, picture_url=PICTURE_URL, gender=GENDER,
                           description=DESCRIPTION, birth_date=BIRTH, expertise=EXPERTISE)
