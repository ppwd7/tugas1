from random import randint
from django.test import TestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

class FunctionalTest(TestCase):
    '''
    ### Status Page Functionality
    1. Accessible from root URL or ROOT/status/
    2. Can post a new status on the page
    3. Every status post is listed on the page
    4. Status on the page is ordered, newest first

    5. Every status message is placed in div selector with panel-body class and
       every div selector with panel-body class contains status message
    6. There is only one textarea on the page and its purpose is to post status message
    7. There is only one submit button on the page and its value is "Post"
    8. Cannot post empty status message
    9. Maximum status message length is 256 characters
    '''

    ROOT_URL = 'http://127.0.0.1:8000/'
    STATUS_URL = 'http://127.0.0.1:8000/status/'
    TEST_MESSAGE = 'FunctionalTest:status#'

    ERROR_EMPTY = 'Status message cannot be empty0'

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.webdriver = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.webdriver.get(self.get_status_page_url())
        super(FunctionalTest, self).setUp()

    def get_status_page_url(self):
        '''
        Returns either root URL or root/status/ URL (1)
        '''
        if randint(0, 10) < 5:
            return self.ROOT_URL
        else:
            return self.STATUS_URL

    def tearDown(self):
        self.webdriver.quit()

    def get_random_status_message(self):
        return self.TEST_MESSAGE + str(randint(0, 65535))

    def post_new_status(self, status_message):
        '''
        Finds the only one textarea and inputs the given
        status_message and then post it (2) (6) (7)
        '''
        textarea = self.webdriver.find_element_by_css_selector('textarea')
        post_button = self.webdriver.find_element_by_xpath("//input[@value='Post']")

        textarea.send_keys(status_message)
        post_button.click()

    def test_update_status_valid_order(self):
        '''
        Tests that all posted status is diplayed and
        the order of status feed is correct (newest first)
        (3) (4) +(5)
        '''
        # post 7 new status
        status_messages = [self.get_random_status_message() for i in range(7)]
        for status in status_messages:
            self.post_new_status(status)

        # find all status messages
        status_list = self.webdriver.find_elements_by_class_name('panel-body') # (5)

        expected_posted_status = status_messages[::-1] # reverse order

        for actual_element, expected in zip(status_list, expected_posted_status):
            self.assertEqual(actual_element.text, expected)

    def test_cannot_post_empty_status(self):
        '''
        User should not be able to post empty message,
        (8)
        '''
        valid_message = self.get_random_status_message()
        self.post_new_status(valid_message)
        self.post_new_status('')

        # the top is the valid one
        top_status = self.webdriver.find_element_by_class_name('panel-body').text
        self.assertEqual(valid_message, top_status) # not the empty one
        for status_element in self.webdriver.find_elements_by_class_name('panel-body'):
            self.assertNotEqual('', status_element.text)

    def test_cannot_post_long_status(self):
        '''
        User should not be able to post very long message,
        (9)
        '''
        long_message = self.get_random_status_message()
        for i in range(300):
            long_message += 'a'
        self.post_new_status(long_message)
        # it will be cut to 256 character because of HTML5 form validation

        # the top is the valid one
        top_status = self.webdriver.find_element_by_class_name('panel-body').text
        self.assertNotEqual(long_message, top_status)
