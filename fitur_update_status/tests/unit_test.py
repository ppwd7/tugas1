from django.test import TestCase, Client
from django.core.urlresolvers import reverse
from fitur_update_status.models import Status
from fitur_update_status.views import StatusView
from fitur_update_status.forms import StatusForm
from tugas1.active_profile import get_active_profile

class UnitTest(TestCase):

    def test_Status_str_representation(self):
        message = 'test message #1'
        user = get_active_profile()
        status = Status.objects.create(message=message, user=user)
        self.assertEqual(str(status), message + ' - ' + user.name)

    def test_post_new_status(self):
        message = 'test message #2'
        response_post = Client().post(reverse('status:index'), {'message':message})
        
        latest_status = Status.objects.all()[len(Status.objects.all()) - 1]
        self.assertEqual(message, latest_status.message) # new object created as requested

        self.assertEqual(response_post.status_code, 201) # added

    def test_status_view_renders_status(self):
        message1 = 'test message #3'
        message2 = 'test message #4'
        Status.objects.create(message=message1)
        Status.objects.create(message=message2)
        response = Client().get(reverse('status:index'))
        response_html = response.content.decode('utf8')
        self.assertIn(message1, response_html)
        self.assertIn(message2, response_html)

    def test_status_update_form_correct_user(self):
        message = 'test message #5'
        form = StatusForm(data={'message':message})
        form.save()
        self.assertNotEqual(len(Status.objects.all()), 0)
        self.assertEqual(Status.objects.all()[len(Status.objects.all()) - 1].user, get_active_profile())

    def test_status_update_form_empty(self):
        form = StatusForm(data={'message':''})
        self.assertFalse(form.is_valid())

    def test_status_update_form_too_long(self):
        long_message = ''
        for i in range(257):
            long_message += 'a'
        form = StatusForm(data={'message':long_message})
        self.assertFalse(form.is_valid())

    def test_post_new_status_empty_message(self):
        response_post = Client().post(reverse('status:index'), {'message':''})
        self.assertEqual(response_post.status_code, 400)
        response_html = response_post.content.decode('utf8')
        self.assertIn(StatusForm.Meta.error_messages['message']['required'], response_html)

    def test_post_new_status_very_long_message(self):
        long_message = ''
        for i in range(StatusView.MAX_STATUS_LENGTH + 1):
            long_message += 'a'
        response_post = Client().post(reverse('status:index'), {'message':long_message})
        self.assertEqual(response_post.status_code, 400)
        response_html = response_post.content.decode('utf8')
        self.assertIn(StatusForm.Meta.error_messages['message']['max_length'], response_html)

    def test_get_latest_status(self):
        message = 'test message #6'
        new_status = Status.objects.create(message=message)
        self.assertEqual(new_status, Status.objects.get_newest())

    def test_get_latest_status_empty(self):
        with self.assertRaises(LookupError):
            Status.objects.get_newest()
