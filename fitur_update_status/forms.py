from django.forms import ModelForm, Textarea
from .models import Status

class StatusForm(ModelForm):
    class Meta:
        model = Status
        fields = ('message',)
        error_messages = {
            'message': {
                'max_length': ("Status message is too long, max 256 characters."),
                'required': ("Status message cannot be empty")
            },
        }
        labels = {
            'message': ('Status Message'),
        }
        widgets = {
            'message': Textarea(attrs={'class':'form-control', 'rows':2}),
        }
