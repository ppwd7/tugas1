from django.shortcuts import render
from django.views import View
from django.http import HttpResponseRedirect, HttpResponseBadRequest
from .models import Status
from tugas1.active_profile import get_active_profile
from .forms import StatusForm

class StatusView(View):
    MAX_STATUS_LENGTH = 256
    TEMPLATE = 'status.html'

    def get(self, request, new_status=False, form=None):
        '''
        Render the status page
        '''
        response = {}
        response['feed'] = []
        response['page_update_status'] = True
        for status in Status.objects.all():
            if status.user == get_active_profile():
                response['feed'].append(status)
        response['feed'] = response['feed'][::-1]
        status_code = 200
        if new_status:
            status_code = 201
        if form:
            response['form'] = form
            status_code = 400
        else:
            response['form'] = StatusForm()
        return render(request, StatusView.TEMPLATE, response, status=status_code)

    def post(self, request):
        '''
        Post new status as active user
        '''
        form = StatusForm(request.POST or None)
        # TODO: add error page
        if not form.is_valid():
            return self.get(request, form=form)
        else:
            form.save()
            return self.get(request, new_status=True)
