from django.apps import AppConfig


class FiturUpdateStatusConfig(AppConfig):
    name = 'fitur_update_status'
