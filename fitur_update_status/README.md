# Update Status Feature

## Status Page Functionality
1. Accessible from root URL or ROOT/status/
2. Can post a new status on the page
3. Every status post is listed on the page
4. Status on the page is ordered, newest first

5. Every status message is placed in div selector with panel-body class and
    every div selector with panel-body class contains status message
6. There is only one textarea on the page and its purpose is to post status message
7. There is only one submit button on the page and its value is "Post"
8. Cannot post empty status message
9. Maximum status message length is 256 characters

## Technical: How to get latest status
```python
from fitur_update_status.models import Status

# May raise LookupError if there is no status!
latest_status = Status.objects.get_newest()
```
