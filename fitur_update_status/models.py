from django.db import models
from tugas1.models import Profile
from tugas1.active_profile import get_active_profile

class StatusManager(models.Manager):
    '''
    Custom model manager for Status model, for easier query
    '''

    def get_newest(self):
        '''
        Returns the last inserted object, NOT based on it's timestamp
        Raises LookupError if there is no status at all
        '''
        if not self.all():
            raise LookupError()
        return self.all()[len(self.all()) - 1]


class Status(models.Model):
    message = models.TextField(max_length=256)
    user = models.ForeignKey(Profile, on_delete=models.PROTECT)
    post_date = models.DateTimeField(auto_now_add=True)
    objects = StatusManager()

    def __str__(self):
        return self.message + ' - ' + self.user.name

    def save(self, *args, **kwargs):
        '''
        Sets the user to active user
        '''
        self.user = get_active_profile()
        super(Status, self).save(*args, **kwargs)
