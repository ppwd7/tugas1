# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-11 17:25
from __future__ import unicode_literals

from django.db import migrations, models
import fitur_add_friend.models


class Migration(migrations.Migration):

    dependencies = [
        ('fitur_add_friend', '0003_auto_20171012_0023'),
    ]

    operations = [
        migrations.AlterField(
            model_name='friend',
            name='created_date',
            field=models.DateTimeField(default=fitur_add_friend.models.Friend.convertTZ),
        ),
    ]
