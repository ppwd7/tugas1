from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from fitur_add_friend.models import Friend
from fitur_add_friend.forms import Add_Friend_Form
from fitur_add_friend.views import index, add_friend, index_url_invalid, delete_friend
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class UnitTest(TestCase):

    def test_friend_url_is_exist(self):
        response = Client().get('/friend/')
        self.assertEqual(response.status_code,200)
    
    def test_friend_using_index_func(self):
        found = resolve('/friend/')
        self.assertEqual(found.func, index)

    def test_friend_using_index_url_invalid_func(self):
        found = resolve('/friend/url_invalid')
        self.assertEqual(found.func, index_url_invalid)

    
    def test_model_can_add_new_friend(self):
        #Creating a new activity
        new_friend = Friend.objects.create(name='mhs_name', url='http://test.herokuapp.com')

        #Retrieving all available activity
        counting_all_friend=Friend.objects.all().count()
        self.assertEqual(counting_all_friend,1)


    def test_form_add_friend_input_has_placeholder_and_css_classes(self):
        form = Add_Friend_Form()
        self.assertIn('class="form-control"', form.as_p())
        self.assertIn('<label for="id_name">name:</label>', form.as_p())
        self.assertIn('<label for="id_url">url:</label>', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Add_Friend_Form(data={'name': '', 'url': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['name'],["This field is required."])
        self.assertEqual(form.errors['url'],["This field is required."])

    def test_form_not_valid_url(self):
        nama = 'wildan fahmi'
        url = 'http://wildan'
        response_post = Client().post('/friend/add_friend', {'name': 'wildan fahmi', 'url': url})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/friend/')
        html_response =  response.content.decode('utf8')
        self.assertFalse(nama in html_response)
        self.assertFalse(url in html_response)

    def test_add_friend_post_success_and_render_the_result(self):
        nama = 'wildan fahmi'
        url = 'http://wildan-test.herokuapp.com'
        response_post = Client().post('/friend/add_friend', {'name': 'wildan fahmi', 'url': url})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/friend/')
        html_response =  response.content.decode('utf8')
        self.assertIn(nama, html_response)
        self.assertIn(url,html_response)

    def test_url_validation(self):
        nama = 'yumna anmuy'
        url = 'http://yumna.herokuapp.com'
        response_post = Client().post('/friend/add_friend', {'name': 'yumna anmuy', 'url': url})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/friend/')
        html_response =  response.content.decode('utf8')
        self.assertFalse(nama in html_response)
        self.assertFalse(url in html_response)

    def test_delete_friend_works(self):
        nama = 'wildan fahmi'
        url = 'http://wildan-test.herokuapp.com'
        response_post = Client().post('/friend/add_friend', {'name': 'wildan fahmi', 'url': url})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/friend/')
        html_response =  response.content.decode('utf8')
        self.assertIn(nama, html_response)
        self.assertIn(url,html_response)

        response= Client().get('/friend/1')

        response= Client().get('/friend/')
        html_response =  response.content.decode('utf8')
        self.assertFalse(nama in html_response)
        self.assertFalse(url in html_response)

    def test_warning_rendered(self):
        response= Client().get('/friend/url_invalid')
        html_response =  response.content.decode('utf8')
        self.assertIn("URL yang anda masukan tidak ada.",html_response)
