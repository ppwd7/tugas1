from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from fitur_add_friend.models import Friend
from fitur_add_friend.forms import Add_Friend_Form
from fitur_add_friend.views import index, add_friend
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class FunctionalTest(TestCase):
    
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_add_friend(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/friend/')
        # find the form element
        nama = selenium.find_element_by_id('id_name')
        url = selenium.find_element_by_id('id_url')

        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        nama.send_keys('Wildan Fahmi')
        url.send_keys('wildan-test.herokuapp.com')

        # submitting the form
        submit.send_keys(Keys.RETURN)

