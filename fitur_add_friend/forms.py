from django import forms

class Add_Friend_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'isinya gak bener tong'
    }

    attrs = {
        'class': 'form-control'
    }

    name = forms.CharField(label='name', required=True, widget=forms.TextInput(attrs=attrs))
    url = forms.URLField(label='url', required=True, widget=forms.URLInput(attrs=attrs), initial='http://')