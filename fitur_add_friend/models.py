from django.db import models
from django.utils import timezone
import pytz

# Create your models here.
class Friend(models.Model):
    def convertTZ():
        return timezone.now()
    
    name = models.CharField(max_length=50)
    url = models.URLField()
    created_date = models.DateTimeField(default=convertTZ)
