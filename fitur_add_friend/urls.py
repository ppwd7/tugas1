from django.conf.urls import url
from .views import index,add_friend,delete_friend,index_url_invalid

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_friend', add_friend, name='add_friend'),
    url(r'^url_invalid', index_url_invalid, name='url_invalid'),
    url(r'^(\d+)$', delete_friend, name='delete_friend')
]