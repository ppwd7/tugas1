from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Friend
from .forms import Add_Friend_Form
import requests


response={}
def index(request):
    friend = Friend.objects.all()
    response['friend']= friend
    response['salah'] =False
    html = 'friend.html'
    response['add_friend_form'] = Add_Friend_Form
    response['page_friends'] = True
    return render(request, html, response)

def index_url_invalid(request):
    friend = Friend.objects.all()
    response['friend']= friend
    response['salah'] =True
    html = 'friend.html'
    response['add_friend_form'] = Add_Friend_Form
    response['page_friends'] = True
    return render(request, html, response)

def add_friend(request):
    form = Add_Friend_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['url'] = request.POST['url']
        check = requests.get(response['url'])
        if(check.status_code == 404):
            return HttpResponseRedirect('/friend/url_invalid')
        friend = Friend(name=response['name'],url=response['url'])
        friend.save()
        return HttpResponseRedirect('/friend/')
    else:
        return HttpResponseRedirect('/friend/')

def delete_friend(request,data_id):
    friend = Friend.objects.get(id=data_id)
    friend.delete()
    return HttpResponseRedirect('/friend/')

