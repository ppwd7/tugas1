from django.apps import AppConfig


class FiturAddFriendConfig(AppConfig):
    name = 'fitur_add_friend'
